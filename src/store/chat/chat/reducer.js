import {createReducer} from '@reduxjs/toolkit';
import {addMessage, deleteMessage, fetchMessages, messagesIsLoading, updateMessage, updateMessageHard} from "./actions";
import {DataStatus} from "../../../common/enums/app/data-status.enum";
import {ActionStatus} from "../../../common/enums/app/action-status.enum";
import {ActionType} from './common';

const initialState = {
    hasErrored: false,
    data: [{
        id: "",
        userId: "",
        avatar: "",
        user: "",
        text: "",
        createdAt: "",
        editedAt: ""
    }],
    isSaving: false,
    status: DataStatus.IDLE,
};

const reducer = createReducer(initialState, (builder) => {
    builder.addCase(fetchMessages.pending, (state) => {
        state.status = DataStatus.PENDING;
    });
    builder.addCase(fetchMessages.fulfilled, (state, {payload}) => {
        const {data} = payload;
        state.data = data;
        state.status = DataStatus.SUCCESS;
    });
    builder.addCase(addMessage.fulfilled, (state, {payload}) => {
        const {data} = payload;
        state.data = state.data.concat(data);
    });

    builder.addCase(updateMessage.fulfilled, (state, {payload}) => {
        const {data} = payload;
        state.data = state.data.map(message => {
            return message.id === data.id ? {...message, ...data} : message;
        });
    });

    builder.addCase(updateMessageHard, (state) => {
        state.isSaving = true;
    })
    builder.addCase(`${updateMessageHard.type}/${ActionStatus.FULFILLED}`, (state, {payload}) => {
        const {data} = payload;

        state.data = state.data.map((it) => {
            return it.id === data.id ? {...it, ...data} : it;
        });
        state.isSaving = false;
    })
    builder.addCase(`${updateMessageHard().type}/${ActionStatus.REJECTED}`, (state) => {
        Object.assign(state, initialState);
    })
    builder.addCase(deleteMessage.fulfilled, (state, {payload}) => {
        const {data} = payload;

        state.data = state.data.filter(message => message.id !== data.id);
    });
    builder.addMatcher((action) => action.type.endsWith(ActionStatus.REJECTED), (state) => {
        state.status = DataStatus.ERROR;
    });
});
export {reducer};
