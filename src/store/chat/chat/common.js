const ActionType = {
    FETCH_MESSAGES: 'messages/fetch-messages',
    ADD_MESSAGE: 'messages/add',
    UPDATE_MESSAGE: 'messages/update',
    HARD_UPDATE: 'messages/hard-update',
    DELETE_MESSAGE: 'messages/delete',
};

export {ActionType};
