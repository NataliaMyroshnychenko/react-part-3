import {all, call, put, delay, takeEvery} from 'redux-saga/effects';
import {ActionStatus} from '../../../common/enums/enums';
import {store} from '../../store';
import {
    messages as messagesService,
    notification as notificationService,
} from '../../../services/services';
import {updateMessageHard, fetchMessages} from './actions';

const HARD_UPDATE_DELAY = 3000;
const RANDOM_HALF = 0.5;

function* changeEntryStatus({payload}) {
    const {message: messagePayload} = payload;

    try {
        const message = yield call([messagesService, messagesService.update], messagePayload);

        yield delay(HARD_UPDATE_DELAY);

        if (Math.random() > RANDOM_HALF) {
            throw new Error('Unexpected error');
        }

        yield put({
            type: `${updateMessageHard.type}/${ActionStatus.FULFILLED}`,
            payload: {
                todo: message,
            },
        });
    } catch (err) {
        yield call(
            [notificationService, notificationService.error],
            'Error',
            err.message,
            {
                onToastrClick: () => store.dispatch(fetchMessages()),
            },
        );

        yield put({
            type: `${updateMessageHard.type}/${ActionStatus.REJECTED}`,
        });
    }
}

function* watchChangeEntryStatus() {
    yield takeEvery(updateMessageHard.type, changeEntryStatus);
}

function* messages() {
    yield all([watchChangeEntryStatus()]);
}

export {messages};
