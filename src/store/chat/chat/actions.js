import {createAction, createAsyncThunk} from '@reduxjs/toolkit';
import { ActionType } from './common';

const fetchMessages = createAsyncThunk(ActionType.FETCH_MESSAGES, async (_args, { extra }) => ({
    data: await extra.messagesService.getAll(),
}));

const addMessage = createAsyncThunk(ActionType.ADD_MESSAGE, async (payload, { extra }) => ({
    data: await extra.messagesService.create(payload),
}));

const updateMessage = createAsyncThunk(ActionType.UPDATE_MESSAGE, async (payload, { extra }) => ({
    data: await extra.messagesService.update(payload),
}));

const updateMessageHard = createAction(ActionType.HARD_UPDATE, (data) => ({
    payload: {
        data
    }
}));

const deleteMessage = createAsyncThunk(ActionType.DELETE_MESSAGE, async (data, { extra }) => {
    await extra.messagesService.delete(data.id);
    return {
        data,
    };
});

const changeStatus = createAsyncThunk(ActionType.UPDATE_MESSAGE, async ({ id, status }, { extra }) => ({
    message: await extra.messagesService.partialUpdate(id, {
        status,
    }),
}));

export {
    fetchMessages,
    addMessage,
    updateMessageHard,
    updateMessage,
    deleteMessage,
    changeStatus
}
