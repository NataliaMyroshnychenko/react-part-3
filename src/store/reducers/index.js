import {combineReducers} from "redux";
import {reducer as messages} from "../chat/chat/reducer";
import editMessage from "../editMessage/reducer";
import {reducer as users} from "../userList/reducer";
import userPage from "../userPage/reducer";
import login from "../login/reducer";

const rootReducer = combineReducers({
    messages,
    editMessage: editMessage,
    users,
    userPage: userPage,
    login
});

export default rootReducer;

