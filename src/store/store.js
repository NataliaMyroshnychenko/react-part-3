import {compose, configureStore} from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import {messages as messagesService} from '../services/services';
import {users as usersService} from '../services/services';
import {rootSaga} from './root-saga';
import rootReducer from "./reducers/index";

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
    reducer: {
        rootReducer
    },
    middleware: (getDefaultMiddleware) => {
        return getDefaultMiddleware({
            thunk: {
                extraArgument: {
                    messagesService,
                    usersService
                },
            },
            serializableCheck: false,
        }).concat(sagaMiddleware);
    },
});
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
composeEnhancers(
    sagaMiddleware.run(rootSaga));

export {store};
