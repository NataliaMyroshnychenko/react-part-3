import {ActionType} from './common';

export const setIsLoginUser = (role) => ({
    type: ActionType.LOGIN_USER,
    payload: {
        role
    }
});
