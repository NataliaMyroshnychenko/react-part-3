import {ActionType} from "./common";

const initialState = {
    isLogin: false,
    role: "none"
};

export default function (state = initialState, action) {
    switch (action.type) {
        case ActionType.LOGIN_USER: {
            const {role} = action.payload;
            return {
                ...state,
                isLogin: true,
                role: role
            };
        }

        default:
            return state;
    }
}
