import {createReducer} from "@reduxjs/toolkit";
import {DataStatus} from "../../common/enums/app/data-status.enum";
import {ActionStatus} from "../../common/enums/app/action-status.enum";
import {addUser, deleteUser, fetchUsers, updateUser, updateUserHard} from "./actions";

const initialState = {
    users: [{
        id: "",
        name: "",
        surname: "",
        email: "",
        password: ""
    }],
    isSaving: false,
    status: DataStatus.IDLE,
};

const reducer = createReducer(initialState, (builder) => {
    builder.addCase(fetchUsers.pending, (state) => {
        state.status = DataStatus.PENDING;
    });
    builder.addCase(fetchUsers.fulfilled, (state, {payload}) => {
        const {users} = payload;
        state.users = users;
        state.status = DataStatus.SUCCESS;
    });
    builder.addCase(addUser.fulfilled, (state, {payload}) => {
        const {users} = payload;
        state.users = state.users.concat(users);
    });

    builder.addCase(updateUser.fulfilled, (state, {payload}) => {
        const {users} = payload;
        state.users = state.users.map(userStore => {
            return userStore.id === users.id ? {...userStore, ...users} : userStore;
        });
    });
    builder.addCase(updateUserHard, (state) => {
        state.isSaving = true;
    })
    builder.addCase(`${updateUserHard.type}/${ActionStatus.FULFILLED}`, (state, {payload}) => {
        const {users} = payload;

        state.users = state.users.map((it) => {
            return it.id === users.id ? {...it, ...users} : it;
        });
        state.isSaving = false;
    })
    builder.addCase(`${updateUserHard().type}/${ActionStatus.REJECTED}`, (state) => {
        Object.assign(state, initialState);
    })

    builder.addCase(deleteUser.fulfilled, (state, {payload}) => {
        const {user} = payload;
        state.users = state.users.filter(userStore => userStore.id !== user.id);
    });

    builder.addMatcher((action) => action.type.endsWith(ActionStatus.REJECTED), (state) => {
        state.status = DataStatus.ERROR;
    });
});

export {reducer};
