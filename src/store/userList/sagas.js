import {all, call, put, delay, takeEvery} from 'redux-saga/effects';
import {ActionStatus} from '../../common/enums/enums';
import {store} from '../store';
import {
    users as usersService,
    notification as notificationService,
} from '../../services/services';
import {updateUserHard, fetchUsers} from './actions';


const HARD_UPDATE_DELAY = 3000;
const RANDOM_HALF = 0.5;

function* changeEntryStatus({payload}) {
    const {user: userPayload} = payload;

    try {
        const user = yield call([usersService, usersService.update], userPayload);

        yield delay(HARD_UPDATE_DELAY);

        if (Math.random() > RANDOM_HALF) {
            throw new Error('Unexpected error');
        }

        yield put({
            type: `${updateUserHard.type}/${ActionStatus.FULFILLED}`,
            payload: {
                user: user,
            },
        });
    } catch (err) {
        yield call(
            [notificationService, notificationService.error],
            'Error',
            err.message,
            {
                onToastrClick: () => store.dispatch(fetchUsers()),
            },
        );

        yield put({
            type: `${updateUserHard.type}/${ActionStatus.REJECTED}`,
        });
    }
}

function* watchChangeEntryStatus() {
    yield takeEvery(updateUserHard.type, changeEntryStatus);
}

function* users() {
    yield all([watchChangeEntryStatus()]);
}

export {users};
