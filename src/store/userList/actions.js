import {ActionType} from './common';
import {createAction, createAsyncThunk} from "@reduxjs/toolkit";

const fetchUsers = createAsyncThunk(ActionType.FETCH_USERS, async (_args, {extra}) => ({
    users: await extra.usersService.getAll(),
}));

const addUser = createAsyncThunk(ActionType.ADD_USER, async (payload, {extra}) => ({
    users: await extra.usersService.create(payload),
}));

const updateUser = createAsyncThunk(ActionType.UPDATE_USER, async (payload, {extra}) => ({
    users: await extra.usersService.update(payload),
}));

const updateUserHard = createAction(ActionType.HARD_UPDATE_USER, (user) => ({
    payload: {
        user
    }
}));

const deleteUser = createAsyncThunk(ActionType.DELETE_USER, async (user, {extra}) => {
    await extra.usersService.delete(user.id);

    return {
        user,
    };
});

const changeStatus = createAsyncThunk(ActionType.UPDATE_USER, async ({id, status}, {extra}) => ({
    message: await extra.usersService.partialUpdate(id, {
        status,
    }),
}));

export {
    fetchUsers,
    addUser,
    updateUser,
    updateUserHard,
    deleteUser,
    changeStatus
}


