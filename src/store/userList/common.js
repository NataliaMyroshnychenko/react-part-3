const ActionType = {
    FETCH_USERS: 'users/fetch-users',
    ADD_USER: 'users/add',
    UPDATE_USER: 'users/update',
    HARD_UPDATE_USER: 'users/hard-update',
    DELETE_USER: 'users/delete',
};

export {ActionType};
