import {SET_CURRENT_MESSAGE_ID, DROP_CURRENT_MESSAGE_ID, SHOW_PAGE_EDIT, HIDE_PAGE_EDIT} from "./actionTypes";

export const setCurrentMessageId = id => ({
    type: SET_CURRENT_MESSAGE_ID,
    payload: {
        id
    }
});

export const dropCurrentMessageId = () => ({
    type: DROP_CURRENT_MESSAGE_ID
});

export const showPageEdit = () => ({
    type: SHOW_PAGE_EDIT
});

export const hidePageEdit = () => ({
    type: HIDE_PAGE_EDIT
});
