import {SET_CURRENT_MESSAGE_ID, DROP_CURRENT_MESSAGE_ID, SHOW_PAGE_EDIT, HIDE_PAGE_EDIT} from "./actionTypes";

const initialState = {
    messageId: '',
    isShownEdit: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_CURRENT_MESSAGE_ID: {
            const {id} = action.payload;
            return {
                ...state,
                messageId: id
            };
        }
        case DROP_CURRENT_MESSAGE_ID: {
            return {
                ...state,
                messageId: ''
            };
        }

        case SHOW_PAGE_EDIT: {
            return {
                ...state,
                isShownEdit: true
            };
        }

        case HIDE_PAGE_EDIT: {
            return {
                ...state,
                isShownEdit: false
            };
        }

        default:
            return state;
    }
}
