import {messages as messageSaga} from '../store/chat/chat/sagas';
import {users as userSaga} from '../store/userList/sagas';
import {all} from 'redux-saga/effects';

function* rootSaga() {
    yield all([messageSaga()]);
    yield all([userSaga()]);
}

export {rootSaga};
