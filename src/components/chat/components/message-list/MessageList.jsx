import PropTypes from 'prop-types';
import Message from '../message/Message';
import './MessageList.css';
import OwnMessage from '../own-message/OwnMessage';

const MessageList = ({onMessageEdit, onMessageDelete, messages}) => {

    const userMessage = (message, i) => {
        const isUser = message.user;
        if (isUser === "Me") {
            return <OwnMessage
                message={message}
                key={i} {...message}
                onMessageDelete={onMessageDelete}
                onMessageEdit={onMessageEdit}
            />;
        }
        return <Message key={i} {...message} />;
    }

    return (
        <div className="messageList">
            {messages?.map((message, i) => (
                userMessage(message, i)
            ))}
        </div>
    );
}

MessageList.propTypes = {
    messages: PropTypes.arrayOf(PropTypes.object)
}

export default MessageList;
