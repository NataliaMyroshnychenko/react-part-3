import PropTypes from 'prop-types';
import moment from 'moment';

import './Header.css';

const Header = ({countParticipants, countMessages, dateTimeLastMessage}) => {

    return (
        <div className="header">
            <div className="header-title">
                <h2>Chat</h2>
            </div>
            <div className="header-users-count">
                {countParticipants} participants
            </div>
            <div className="header-messages-count">
                {countMessages} messages
            </div>
            <div className="header-last-message-date">
                last message at {moment((dateTimeLastMessage)).format('DD.MM.yyyy HH:mm')}
            </div>
        </div>
    );
};

Header.propTypes = {
    countParticipants: PropTypes.number,
    countMessages: PropTypes.number,
    dateTimeLastMessage: PropTypes.string
};

export default Header;
