import logo from './processing.gif';
import './Preloader.css';
import {DataStatus} from "../../../../common/enums/app/data-status.enum";

const Preloader = ({status}) => {

    const getPreloader = () => {

        return (
            <div className="preloader">
                <img id="preloader-id" src={logo} className="preloader-logo" alt="logo"/>
            </div>);
    }

    if (status !== DataStatus.SUCCESS) {
        return getPreloader()
    } else {
        return null;
    }
}

export default Preloader;
