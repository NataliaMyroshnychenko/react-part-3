import {useState} from '../../../../hooks/hooks';
import PropTypes from 'prop-types';

import './Message.css';
import moment from 'moment';

const Message = ({avatar, user, text, createdAt}) => {

    const [black, setBlack] = useState(false);

    const changeColor = () => {
        setBlack(!black);
    }

    let btn_class = black ? "blackButton" : "whiteButton";

    return (
        <div className="message">
            <div className="message-user-avatar">
                <img src={avatar} alt="Аватар"/>
            </div>
            <div className="message-user-name">
                {user}
            </div>
            <div className="message-text">
                {text}
            </div>
            <div className="message-time">
                {moment(createdAt).format('HH:mm')}
            </div>
            <div className="message-like">
                <button className={btn_class}
                        onClick={changeColor}>
                    Like
                </button>
            </div>
        </div>
    );
};

Message.propTypes = {
    user: PropTypes.string,
    text: PropTypes.string.isRequired,
    createdAt: PropTypes.string,
}

export default Message;
