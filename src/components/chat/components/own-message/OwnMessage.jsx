import PropTypes from 'prop-types';
import moment from 'moment';
import './OwnMessage.css';

const OwnMessage = ({onMessageEdit, onMessageDelete, text, createdAt, id, message}) => {

    const handleFormEdit = (event) => {
        event.preventDefault();
        onMessageEdit(id);
    }

    const handleDeleteMessage = (event) => {
        event.preventDefault();
        onMessageDelete(message);
    }

    return (
        <div className="own-message">
            <div className="message-text">
                {text}
            </div>
            <div className="message-time">
                {moment(createdAt).format('HH:mm')}
            </div>
            <div className="message-edit">
                <button onClick={handleFormEdit}>
                    Edit
                </button>
            </div>
            <div className="message-delete">
                <button
                    onClick={handleDeleteMessage}
                >
                    Delete
                </button>
            </div>
        </div>
    )
}

OwnMessage.propTypes = {
    id: PropTypes.string,
    user: PropTypes.string,
    text: PropTypes.string.isRequired,
    createdAt: PropTypes.string,
    messages: PropTypes.arrayOf(PropTypes.object),
}

export default OwnMessage;
