import {useState, useDispatch} from '../../../../hooks/hooks';
import {addMessage} from '../../../../store/chat/chat/actions';
import service from '../../../../store/chat/chat/service';
import './MessageInput.css';

const MessageInput = () => {
    const [message, setMessage] = useState({
        id: "",
        userId: "",
        avatar: "",
        user: "",
        text: "",
        createdAt: "",
        editedAt: ""
    });
    const [input, setInput] = useState({Input: ''});
    const dispatch = useDispatch();

    const onSaveMessage = (event) => {
        event.preventDefault()
        dispatch(addMessage(message));
        setInput({Input: ''});
    }

    const onChangeData = (e) => {
        const value = e.target.value;
        setMessage({
                "id": service.getNewId(),
                "userId": "Me",
                "avatar": "",
                "user": "Me",
                "text": value,
                "createdAt": new Date().toISOString(),
                "editedAt": ""
            }
        );
        setInput({...input, [e.target.name]: e.target.value});
    };

    return (
        <form className="message-input" onSubmit={onSaveMessage}>
            <div className="message-input-text">
                <input
                    value={input.Input}
                    type="text"
                    placeholder="Enter your message..."
                    name={'Input'}
                    onChange={onChangeData}
                />
            </div>
            <div className="message-input-button">
                <button onClick={onSaveMessage}>
                    Send
                </button>
            </div>
        </form>
    );
}

export default MessageInput;




