import React, {Component} from 'react'
import logo from '../../../assets/logo.jpg';
import {connect} from 'react-redux'
import * as actions from '../../../store/chat/chat/actions';
import {setCurrentMessageId, showPageEdit} from '../../../store/editMessage/actions';
import './index.css';
import Header from '../components/header/Header.jsx';
import MessageInput from '../components/message-input/MessageInput.jsx';
import MessageList from '../components/message-list/MessageList.jsx';
import Preloader from '../components/preloader/Preloader';
import defaultStoreConfig from '../../shared/config/defaultStoreConfig.json';
import '../components/preloader/Preloader.css';
import {deleteMessage, fetchMessages} from "../../../store/chat/chat/actions";
import EditMessage from "../../editMessage";

class Chat extends Component {
    constructor(props) {
        super(props)
        this.onEdit = this.onEdit.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onAdd = this.onAdd.bind(this);
        this.state = this.getDefaultUserData();
    }

    componentDidMount() {
        this.props.fetchMessages();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.id !== this.props.id) {
            const message = this.props.messages.find(message => message.id === nextProps.id);
            this.setState(message);
        }
    }

    getDefaultUserData() {
        return {
            ...defaultStoreConfig
        };
    }

    handlerCountParticipants = () => {
        const list = new Set();
        this.props.messages.data?.map((message, i) => {
            list.add(message.user);
        });
        return list.size;
    }

    handlerCountMessages() {
        return this.props.messages.data?.length;
    }

    handlerDateTimeLastMessage = () => {
        var messages = this.props.messages.data;
        if (messages != undefined) {
            var lastMessage = this.props.messages?.data[this.props.messages.data.length - 1];
            var lastdate = lastMessage?.createdAt;
            return lastdate;
        } else {
            return null;
        }
        return "";
    }

    sortByDate(messages) {
        var sortMessages = messages.sort(function (a, b) {
            var c = new Date(a.createdAt).getTime();
            var d = new Date(b.createdAt).getTime();
            if (c > d) {
                return 1;
            }
            if (c < d) {
                return -1;
            }
            return 0;
        });
        return sortMessages;
    }

    onEdit(id) {
        this.props.setCurrentMessageId(id);
        this.props.showPageEdit();
    }

    onDelete(message) {
        this.props.deleteMessage(message);
    }

    onAdd() {
        this.props.showPageEdit();
    }

    render() {
        if (this.props.hasErrored) {
            return <p>Sorry! There was an error loading the items</p>;
        }

        return (

            <div className="chat">
                <Preloader status={this.props.status}/>
                <Header
                    countParticipants={this.handlerCountParticipants()}
                    countMessages={this.handlerCountMessages()}
                    dateTimeLastMessage={this.handlerDateTimeLastMessage()}
                    messages={this.state.messages.data}
                />
                <MessageList
                    messages={this.props.messages.data}
                    onMessageDelete={this.onDelete}
                    onMessageEdit={this.onEdit}
                />
                <MessageInput/>
                <EditMessage messages={this.props.messages.data}/>

                <header className="logo-wrapper">
                    <img src={logo} className="main-logo" alt="logo"/>
                </header>
            </div>
        );
    }
}

const mapStateToProps = (state) => {

    return {
        messages: state.rootReducer.messages,
        data: state.rootReducer.messages.data,
        isShownEdit: state.isShownEdit,
        status: state.rootReducer.messages.status
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        ...actions,
        setCurrentMessageId: (id) => dispatch(setCurrentMessageId(id)),
        showPageEdit: () => dispatch(showPageEdit()),
        fetchMessages: () => dispatch(fetchMessages()),
        deleteMessage: (message) => dispatch(deleteMessage(message))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
