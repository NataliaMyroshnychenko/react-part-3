
const UserItem = ({id, name, surname, email, key, onDelete, onEdit, user}) => {

    return (
        <div className="container list-group-item">
            <div className="row">
                <div className="col-8">
                    <span className="badge badge-secondary float-left"
                          style={{fontSize: "2em", margin: "2px"}}>{name} {surname}</span>
                    <span className="badge badge-info" style={{fontSize: "2em", margin: "2px", color: "black"}}>{email}</span>
                </div>
                <div className="col-4 btn-group">
                    <button className="btn btn-outline-primary" onClick={(e) => onEdit(id)}> Edit</button>
                    <button className="btn btn-outline-dark" onClick={(e) => onDelete(user)}> Delete</button>
                </div>
            </div>
        </div>
    );
};

export default UserItem;
