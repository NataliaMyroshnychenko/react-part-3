import './EmailInput.css';
import PropTypes from 'prop-types';
import {useDispatch, useState} from "../../../../hooks/hooks";

const EmailInput = ({keyword, text, type, label, onChange}) => {

    const [isValid, setIsValid] = useState(true);
    const dispatch = useDispatch();

    const onBlur = (e) => {
        const isValid = e.target.value.includes('@');
        setIsValid(isValid);
    }

    const getErrorMessage = () => {
        return <span className='error-message'>That is not a valid email</span>;
    }

    const inputClass = isValid ? 'col-sm-9' : 'error col-sm-9';

    return (
        <div className='email-input form-group row'>
            <label className="col-sm-3 col-form-label">{label}</label>
            <input
                value={text}
                type={type}
                className={inputClass}
                onChange={e => onChange(e, keyword)}
                onBlur={onBlur}
            />
            {!isValid ? getErrorMessage : null}
        </div>
    );
}

EmailInput.propTypes = {
    text: PropTypes.string,
    type: PropTypes.string,
    keyword: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func
};

export default EmailInput;
