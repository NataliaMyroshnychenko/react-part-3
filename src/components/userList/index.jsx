import UserItem from './UserItem';
import {setCurrentUserId, showPage} from '../../store/userPage/actions';
import {useDispatch, useEffect, useSelector} from "../../hooks/hooks";
import {deleteUser, fetchUsers} from "../../store/userList/actions";
import {DataStatus} from "../../common/enums/app/data-status.enum";
import Preloader from "../chat/components/preloader/Preloader";
import UserPage from "../userPage";
import React from "react";

const UserList = () => {

    const {users, status,} = useSelector(({rootReducer}) => ({
        users: rootReducer.users.users,
        status: rootReducer.users.status,
    }));

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchUsers());
    }, [dispatch]);

    const onEdit = (id) => {
        dispatch(setCurrentUserId(id));
        dispatch(showPage());
    }

    const onDelete = (id) => {
        dispatch(deleteUser(id));
    }

    const onAdd = () => {
        dispatch(showPage());
    }
    return (

        <div className="row">
            <Preloader status={status}/>
            <div className="list-group col-10">
                {
                    users.map(user => {
                        return (
                            <UserItem
                                // key={user.id}
                                id={user.id}
                                name={user.name}
                                surname={user.surname}
                                email={user.email}
                                onEdit={onEdit}
                                onDelete={onDelete}
                                user={user}
                            />
                        );
                    })
                }
                <UserPage users={users}/>
            </div>
            <div className="col-2">
                <button
                    className="btn btn-success"
                    onClick={onAdd}
                    style={{margin: "5px"}}
                >
                    Add user
                </button>
            </div>
        </div>
    );
}

export default UserList;
