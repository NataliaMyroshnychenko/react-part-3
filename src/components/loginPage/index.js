import React, {Component} from "react";
import {connect} from 'react-redux'
import * as actions from '../../store/userPage/actions';
import {fetchUsers} from '../../store/userList/actions';
import TextInput from '../userList/inputs/text/TextInput';
import PasswordInput from '../userList/inputs/password/PasswordInput';
import EmailInput from '../userList/inputs/email/EmailInput';
import loginFormConfig from '../shared/config/loginFormConfig.json';
import {setIsLoginUser} from "../../store/login/actions";

class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = this.getDefaultUserData();
        this.onChangeData = this.onChangeData.bind(this);
        this.onLogin = this.onLogin(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.userId !== this.props.userId) {
            const user = this.props.users.users.find(user => user.id === nextProps.userId);
            this.setState(user);
        }
    }

    onLogin() {
        fetchUsers();
        const user = this.props.users.users
            .filter(user => (user.email === this.state.email
                && user.password === this.state.password));

        if (user === undefined) {
            return (
                <div>user not found</div>
            )
        }
        this.props.setIsLoginUser();
        this.setState(this.getDefaultUserData());
        return <div>user found</div>
    }

    onChangeData(e, keyword) {
        const value = e.target.value;
        this.setState(
            {
                ...this.state,
                [keyword]: value
            }
        );
    }

    getDefaultUserData() {
        return {
            ...loginFormConfig
        };
    }

    getInput(data, {label, type, keyword}) {
        switch (type) {
            case 'text':
                return (
                    <TextInput
                        label={label}
                        type={type}
                        text={data[keyword]}
                        keyword={keyword}
                        onChange={this.onChangeData}
                    />
                );
            case 'email':
                return (
                    <EmailInput
                        label={label}
                        type={type}
                        text={data[keyword]}
                        keyword={keyword}
                        innerRef="email"
                        onChange={this.onChangeData}
                    />
                );
            case 'password':
                return (
                    <PasswordInput
                        label={label}
                        type={type}
                        text={data[keyword]}
                        keyword={keyword}
                        onChange={this.onChangeData}
                    />
                );
            default:
                return null;
        }
    }

    getUserPageContent() {
        const data = this.state;

        return (
            <div className="modal" style={{display: "block"}} tabIndex="-1" role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content" style={{padding: "5px"}}>
                        <div className="modal-header">
                            <h5 className="modal-title">Login</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"
                                    onClick={this.onCancel}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            {
                                loginFormConfig.map(item => this.getInput(data, item))
                            }
                        </div>
                        <div className="modal-footer">
                            <button className="btn btn-primary" onClick={this.onLogin}>Sign in</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return this.getUserPageContent();
    }
}

const mapStateToProps = (state) => {
    return {
        users: state.rootReducer.users,
        isShown: state.rootReducer.userPage.isShown,
        userId: state.rootReducer.userPage.userId
    }
};

const mapDispatchToProps = {
    ...actions,
    fetchUsers,
    setIsLoginUser
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
