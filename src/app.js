import Chat from "./components/chat/chat";
import EditMessage from "./components/editMessage";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import React, {Component, Suspense,} from 'react';
import UserList from "./components/userList";
import UserPage from "./components/userPage";
import LoginPage from "./components/loginPage";

class App extends Component {
    render() {
        return (
            <Router>
                <Suspense fallback={<div>Loading...</div>}>
                    <Switch>
                        <Route exact path="/" component={ (()=>(<LoginPage/>)) } />
                        <Route path="/messages" component={(() => (<Chat/>))}/>
                        <Route path="/message-editor" component={(() => (<EditMessage/>))}/>
                        <Route path="/user-list" component={(() => (<UserList/>))}/>
                        <Route path="/user-editor" component={(() => (<UserPage/>))}/>
                    </Switch>
                </Suspense>
            </Router>
        );
    }
}

export default App;
