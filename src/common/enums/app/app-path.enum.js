const AppPath = {
  ROOT: '/',
  MESSAGES: '/messages',
  MESSAGES_$ID: '/messages/:id',
  ANY: '*',
};

export { AppPath };
