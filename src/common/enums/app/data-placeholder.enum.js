const DataPlaceholder = {
  NO_MESSAGES: 'There are no messages or the tasks don\'t match the selected filters',
  NO_MESSAGE: 'Opps... there is no such todo',
  PAGE_NOT_FOUND: 'Oops... page not found',
};

export { DataPlaceholder };
