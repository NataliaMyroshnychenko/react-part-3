const MessageKey = {
    ID: 'id',
    USER_ID: 'userId',
    AVATAR: 'avatar',
    USER: 'user',
    TEXT: 'text',
    CREATED_AT: 'createdAt',
    EDITED_AT: 'editedAt'
};
export {MessageKey};
