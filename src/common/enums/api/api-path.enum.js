const ApiPath = {
  MESSAGES: '/messages',
  USERS: '/users',
};

export { ApiPath };
