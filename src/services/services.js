import {ENV} from '../common/enums/enums';
import {Http} from './http/http.service';
import {Notification} from './notification/notification.service';
import {Messages} from './messages/messages.service';
import {Users} from "./messages/users.service";

const http = new Http();
const notification = new Notification();
const messages = new Messages({
    baseUrl: ENV.API.URL,
    http,
});
const users = new Users({
    baseUrl: ENV.API.URL,
    http,
});

export {http, notification, messages, users};
